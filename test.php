<?php 
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User; 
use Spatie\Permission\Traits\HasRoles;
use DB,File,Hash,Input,Redirect,Response,Session,URL,View,Config;
use Auth;
use Storage;
use App\Models\UserInformation; 
use App\Models\Country; 
use App\Models\Order; 
use App\Models\OrderStatus;
use App\Models\Wallet;
use Mail;


class OrderController extends Controller {   


	/**
		* Create a new controller instance.
		*
		* @return void
	*/

	public function __construct() { 
		$this->middleware('auth', ['except' => []]);
	}
	  
	
	public function orders(Request $request) { 
		
		$get_user_id = get_userid_role();
		
		if($request->session()->has('ordersearch') and (isset($_GET['page']) and $_GET['page']>=1) OR (isset($_GET['s']) and $_GET['s'])) {
			$_POST = $request->session()->get('ordersearch');
		}else{
			$request->session()->forget('ordersearch');
		}
	
		$query = Order::query()->leftjoin('users', 'users.id', '=', 'orders.user_id');
		
		
		if(! empty($_POST)){
			if(isset($_POST['search']) and $_POST['search'] !=''){
				$search = $_POST['search'];
				$request->session()->put('ordersearch.search', $search); 
				
				$query = $query->where(function($query) use($search){
					$query->where('users.name', 'like', '%' . $search . '%') 
						  ->orWhere('users.email', 'like', '%' . $search . '%');
				}); 
			}
		} else {
			$request->session()->forget('ordersearch');
		} 
		
		$query = $query->whereIn('orders.merchant_id',$get_user_id);  
		
	
        $query = $query->select('orders.*','users.name as user_name')->whereIn('orders.status',array(1,2,3,4))->orderBy('orders.status','ASC')->orderBy('orders.id','DESC')->paginate(20);   
		
		
		$order_data = array();
		
		if(!$query->isEmpty()) { 
		
			foreach($query as $key=>$query_val) {
		
				$order_status = DB::table('order_status')->where('order_id',$query_val->id)->orderBy('id','ASC')->get();
			
				$order_item_data = array();
				$order_items = DB::table('order_items')->where('is_addon',0)->where('order_id',$query_val->id)->orderBy('id','ASC')->get(); 
				if(!empty($order_items)) { 
					foreach($order_items as $key=>$order_val) {
						$order_items_addon = DB::table('order_items')->where('is_addon',1)->where('menu_id',$order_val->menu_id)->where('order_id',$query_val->id)->orderBy('id','ASC')->get(); 
						$order_item_data[$key] = (array) $order_val;
						$order_item_data[$key]['addon_item'] = $order_items_addon;
					}
				}	
				
				$get_order_status = $query_val->status;
			
				$reataurantquery = User::query();
				$reataurantquery->with(['logo','user_information'])
				->leftjoin('user_informations', 'user_informations.user_id', '=', 'users.id')
				->leftjoin('countries', 'countries.id', '=', 'user_informations.country_id');   
				$reataurant_data = $reataurantquery->select('users.id','users.parent_id','countries.name as country_name')
				->where('users.id',$query_val->merchant_id)->groupBy('users.parent_id')->groupBy('users.id')->first(); 	 
  
				$data = array();
				$data['id'] = $query_val->id;
				$data['timeslots'] = $query_val->timeslots;
				$data['order_date'] = $query_val->created_at;
				$data['wallet_amount'] = $query_val->wallet_amount;
				$data['promocode'] = $query_val->promocode;
				$data['promocode_amount'] = $query_val->promocode_amount;
				$data['point'] = $query_val->point;
				$data['point_amount'] = $query_val->point_amount;
				$data['tax_percentage'] = $query_val->tax_percentage;
				$data['tax_amount'] = $query_val->tax_amount;
				$data['tips_amount'] = $query_val->tips_amount;
				$data['amount'] = $query_val->amount;
				$data['total_amount'] = $query_val->total_amount; 
				$data['status'] = $query_val->status;  	
				$data['user_name'] = $query_val->user_name;  	
				$data['order_item_data'] = $order_item_data;  
				$data['order_status'] = $order_status;  
				$data['reataurant_data'] = $reataurant_data; 
				
				$order_data[$get_order_status][] = $data;
				
			
			}
		} 
		
		//echo '<pre>'; print_r($order_data); die; 
		
		return view('admin.orders.orders',compact('order_data','query')); 
	} 
	
	
	public function status_change_menu(Request $request) {    
	
		$data = Order::where('id',$id)->first();
		
		$get_role_id = !empty(auth()->user()->role_id)?auth()->user()->role_id:'';
		$get_parent_id = !empty(auth()->user()->parent_id)?auth()->user()->parent_id:'';
		if($get_role_id==3) {
			$query = $query->where(function ($query) use ($get_parent_id) {
				$query->where('user_id',Auth::id())->orWhere('user_id',$get_parent_id);
			});
		} else {  
			$query = $query->where('user_id', Auth::id()); 
		}
	
		$query = $query->first();  
		
		$get_parent_id = $request->parent_id; 
		if(!empty($get_parent_id)) {
		
			$get_menu_status = MenuStatus::where('user_id',Auth::id())->where('menu_id',$request->id)->first();
			if(empty($get_menu_status)) {
				$menu_status = array();
				$menu_status['menu_id'] = $request->id;
				$menu_status['user_id'] = Auth::id();
				$menu_status['status'] = ($query->status==1)?0:1;
				$get_menu_status = MenuStatus::create($menu_status);
			} else {
			
				if($get_menu_status->status) {
					$get_menu_status->status = 0; 
				} else {
					$get_menu_status->status = 1;
				}  
				$get_menu_status->save();
			} 
			
			return json_encode(["reason"=>"Menu display changed!","success"=>true,'status'=>$get_menu_status->status]); 
		
		} else { 
			
			if($query) { 
				if($query->status) {
					$query->status = 0; 
				} else {
					$query->status = 1;
				} 
				$query->save();  
				return json_encode(["reason"=>"Menu display changed!","success"=>true,'status'=>$query->status]); 
			} 
		} 
	}
	
	
	public function orders_status_update(Request $request) { 
	
		$get_order_id = $request->order_id;
		$status = $request->order_status;
		$cancel_reason = $request->cancel_reason;
	
        $data = Order::where('id',$get_order_id)->first();
		
		if(empty($data)) {
			return json_encode(["reason"=>"Order could not be updated status successfully!","success"=>false,'status'=>$status]); 
		}  
		
		$get_status = Order::where('id', $get_order_id)->update(array('status' => $status,'cancel_reason' => $cancel_reason));
		
		if($get_status && !in_array($data->status,array(5,6))) { 
		 
			$order_status = array();
			$order_status['user_id'] = $data->merchant_id; 
			$order_status['status'] = $status;
			$order_status['order_id'] = $data->id;; 
			OrderStatus::create($order_status); 
			
			// Refound Amount 
			if(in_array($status,array(5,6))) {
				$wallet_amount_data = !empty($data->total_amount)?$data->total_amount:0; 
				if($wallet_amount_data>0) {
					$user_wallet = array();
					$user_wallet['user_id'] = $data->user_id;
					$user_wallet['reataurant_id'] = $data->merchant_id; 
					$user_wallet['wallet_amt'] = $wallet_amount_data; 
					Wallet::create($user_wallet); 
				}
			}
			
			$get_message = '';
			if($status==5) {
				$get_message = 'Oops! We’re sorry your order has been rejected by the outlet due to '.$cancel_reason.'. The amount paid will be automatically credited to your Essence Takeout wallet which you can use to place an order with another outlet or item.';
			}
			if($status==6) {
				$get_message = 'Oops! We’re sorry your order has been cancelled by the outlet due to '.$cancel_reason.'. The amount paid will be automatically credited to your Essence Takeout wallet which you can use to place an order with another outlet or item.';
			} 
			
			$get_user = DB::table('users')->where('id',$data->user_id)->first();
			$device_token = !empty($get_user->device_token)?$get_user->device_token:'';
			$device_type = !empty($get_user->device_type)?$get_user->device_type:'';
			if($device_token && $device_type=='iphone' && $get_message) { 
				send_notification_iphone($device_token,$get_message);
			}			
			
			return json_encode(["reason"=>"Order status has been changed successfully.","success"=>true,'status'=>$status]); 
			
		} else  {
		
			return json_encode(["reason"=>"Order could not be updated status successfully!","success"=>false,'status'=>$status]); 
		}		
	}	
	
	public function orders_status(Request $request,$status=null,$id=null) { 
	
        $data = Order::where('id',$id)->first();
		
		if(empty($data)) {
			session()->flash('message','Order could not be updated status successfully!');
			session()->flash('level','danger'); 
			return redirect('admin/orders');				
		} 

		$get_status = Order::where('id', $id)->update(array('status' => $status)); 
		if($get_status) {
		
			$user_information_data = DB::table('user_informations')->where('user_id',$data->merchant_id)->first(); 
			$restaurant_name_title = !empty($user_information_data->reataurant_name)?$user_information_data->reataurant_name:'Essence';
		
			// order_status
			
			$order_status = array();
			$order_status['user_id'] = $data->merchant_id; 
			$order_status['status'] = $status;
			$order_status['order_id'] = $data->id;; 
			OrderStatus::create($order_status); 
			
			// Refound Amount 
			if(in_array($status,array(5,6))) {
				$wallet_amount_data = !empty($data->total_amount)?$data->total_amount:0; 
				if($wallet_amount_data>0) {
					$user_wallet = array();
					$user_wallet['user_id'] = $data->user_id;
					$user_wallet['reataurant_id'] = $data->merchant_id; 
					$user_wallet['wallet_amt'] = $wallet_amount_data; 
					Wallet::create($user_wallet); 
				}
			}
			
			$get_message = '';
			if($status==2) {
				$get_message = $restaurant_name_title.' has accepted your order. Your order is being prepared and will be ready by the scheduled time slot.';
			}
			if($status==3) {
				$get_message = 'Hi There! Your order is fresh and ready for take-out.';
			}
			if($status==4) {
				$get_message = 'Thank you for using Essence Takeout. We hope you enjoy your takeaway!';
			}
			
			$get_user = DB::table('users')->where('id',$data->user_id)->first();
			$device_token = !empty($get_user->device_token)?$get_user->device_token:'';
			$device_type = !empty($get_user->device_type)?$get_user->device_type:'';
			if($device_token && $device_type=='iphone' && $get_message) { 
				send_notification_iphone($device_token,$get_message);
			} 
 
			// Email
			if($status==4) {

				$get_orders = DB::table('orders')->where('id',$id)->first();
				$get_merchant_name = DB::table('users')->select('name')->where('id',$data->merchant_id)->first();
				$user_address = DB::table('user_informations')->where('user_id',$data->merchant_id)->first();
				$user_address_val = '';
				if(!empty($user_address)) {
					$user_address_val = $user_address->address_line.', '.$user_address->district.', '.$user_address->city_state.', '.$user_address->adderss_pincode;
				}			

				$subject = 'Your Essence Takeout order summary for Order No. '.$get_orders->id; 

				$data = array();
				$data['name'] = $get_user->name;
				$data['order_no'] = $get_orders->id;
				$data['order_placed'] = date('d M Y, h:i A',strtotime($get_orders->created_at));
				$data['order_picked'] = date('d M Y, h:i A');
				$data['order_status'] = Config::get('constants.ORDER_STATUS.'.$get_orders->status);
				$data['ordered_from_name'] = !empty($get_merchant_name->name)?$get_merchant_name->name:'';
				$data['ordered_from_address'] = $user_address_val;
				$data['item_name_arr'] = order_id_to_order_data($get_orders->id);  
				
				//$html_email = view('frontend.emails.order', array('data'=>$data,'get_orders'=>$get_orders)); 
				//$html_email = mb_convert_encoding($html_email, 'UTF-8', 'UTF-8'); 
				
				$useremail = $get_user->email;
				if(Config::get('constants.GET_HTTP_HOST')!='localhost') {  
					$mail = Mail::send('frontend.emails.order', array('data'=>$data,'get_orders'=>$get_orders), function($message) use ($subject,$useremail){    
						$message->to($useremail);
						$message->subject($subject);    
					});   
				}
			}


		
			return redirect('admin/orders')->with('message', "Order status has been changed successfully."); 
		} else  {
			return redirect('admin/orders')->with('errormessage', "Order could not be updated status successfully!"); 
		}		
	}
	
}